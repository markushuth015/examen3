<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>
<?php 
  class Libro {
    private string $titulo;
    private $tipo;
    private string $editorial;
    private int $año;
    private $ISBN;
    private $autores = array();

    function __construct($titulo, $tipo,$editorial,$año,$ISBN,$autores) {
        $this->titulo = $titulo;
        $this->tipo = $tipo;
        $this->editorial = $editorial;
        $this->año = $año;
        $this->ISBN= $ISBN;
        $this->autores= $autores;
    }
    public function getTitulo() {
        return $this->titulo;
    }

    public function getTipo() {
        return $this->tipo;
    }

    public function getEditorial() {
        return $this->editorial;
    }

    public function getYear() {
        return $this->año;
    }

    public function getISBN() {
        return $this->ISBN;
    }

    public function getAutores() {
        return $this->autores;
    }

 }
 class Autor {

    private string $nombre;
    private string $nacionalidad;
    private $nacimiento;


    public function __construct(string $nombre, string $nacionalidad, $nacimiento) {
        $this->nombre = $nombre;
        $this->nacionalidad = $nacionalidad;
        $this->f_nacimiento = $nacimiento;
    }

    public function getNombre() {
        return $this->nombre;
    }
    public function getNacionalidad() {
        return $this->nacionalidad;
    }
    public function getNacimiento() {
        return $this->nacimiento;
    }

    }
    $autor1 = new Autor("Markus", "Paraguayo", date('d-m-Y', strtotime('01-01-2000')));
    $autor2 = new Autor("Juan", "Brazuca", date('d-m-Y', strtotime('01-01-2000')));
    $libro1 = new Libro("La vida de markus", "vida", "markus", 2022, "6541698468", array($autor1, $autor2));
    $Lista = $libro1->getAutores();
    $libroNombre = $libro1->getTitulo();

    print("Los autores del mejor libro del mundo  $libroNombre son: \n");
    foreach ($Lista as $key => $value) {
    $nombreautores = $value->getNombre();
    print("$nombreautores, \n");

}
?>
    
</body>
</html>
